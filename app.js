let express = require('express');
let app = express();
let config = require('./config');
let cookieParser = require('cookie-parser');
let session = require('express-session');
let fs = require('fs');
let passport = require('passport');
let Auth0Strategy = require('passport-auth0');

// Configure express
// Set up cookies and sessions to save tokens
app.use(cookieParser());
app.use(session(
  {
    secret: '0dc529ba-5051-4cd6-8b67-c9a901bb8bdf',
    resave: false,
    saveUninitialized: false
  }));

// Configure Passport to use Auth0
let strategy = new Auth0Strategy(
  {
    domain: config.SAML.domain,
    clientID: config.SAML.clientID,
    clientSecret: config.SAML.clientSecret,
    callbackURL: config.SAML.redirectURI
  },
  function (accessToken, refreshToken, extraParams, profile, done) {
    // accessToken is the token to call Auth0 API (not needed in the most cases)
    // extraParams.id_token has the JSON Web Token
    // profile has all the information from the user
    return done(null, profile);
  }
);

passport.use(strategy);

app.use(passport.initialize());
app.use(passport.session());

app.get('/login', passport.authenticate('auth0', {
  scope: config.SAML.scopes.join(' ')
}), function (req, res) {
  res.redirect('/');
});

app.get('/startSession', (req, res, next) => {
  passport.authenticate('auth0', function (err, user, info) {
    if (user) {
      req.session.displayName = user.displayName;
      res.redirect('/');
    }
  })(req, res, next);
});

app.get('/', function (req, res, next) {
  // show the loggedIn page only when the user is logged in, else prompt to login
  if (!req.session.displayName) {
    let tmpl = fs.readFileSync('./index.html');
    let resData = tmpl.toString().replace('{AUTH_URI}', '/login');
    
    res.writeHead(200, {'Content-Type': 'text/html', 'Content-Length': resData.length});
    res.write(resData);
    res.end();
  } else {
    let tmpl = fs.readFileSync('./loggedIn.html');
    let resData = tmpl.toString().replace('{USER}', req.session.displayName);
    
    res.writeHead(200, {'Content-Type': 'text/html', 'Content-Length': resData.length});
    res.write(resData);
    res.end();
  }
});

app.get('/logout', async (req, res) => {
  req.session.destroy();
  res.redirect('/');
});

let server = app.listen(config.port, function () {
  let host = server.address().address;
  let port = server.address().port;
  
  console.log('Listening at http://%s:%s', host, port);
});